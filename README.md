# FuseMap

## Dependencies

- [Topojson](https://github.com/mbostock/topojson)
- [D3](http://www.d3js.org)
- [Lodash](http://www.lodash.com)

## Usage

1) [Download](https://bitbucket.org/jpablolomeli/fusemap/downloads/fusemap-0.1.0.zip) the toolkit.

2) Unzip the file, you'll get the files in the following direcotry structure:

![Directory structure](https://bytebucket.org/jpablolomeli/fusemap/raw/4e58c608b1451edf151979ad5ec41e7738599af5/images/dir_structure.jpg?token=86deffbb3d4ae66643c45b7197edf7b28f626f0e)

3) Include the references to the files in your html, modify according to your own structure:

```
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<script src="js/vendor/lodash.js"></script>
	<script src="js/vendor/d3.js"></script>
	<script src="js/vendor/topojson.js"></script>
	<script src="js/FuseMap.js"></script>
</body>
</html>
```

4) Profit!

```
<div id="mapChart"></div>

<!-- <script> references... -->

<script>
var configMap = {
  selector: 'maps',
  fuseData: 'data/mapData.json',
  mapData: 'maps/cb_2013_us_county_500k.topo.json',
  mapPropertyId: 'COUNTYFP',
  colorBrewerScheme: [
    'rgb(240,249,232)',
    'rgb(123,204,196)',
    'rgb(8,104,172)'
  ],
  showTooltip: true,
  showLimitLines: true,
  showLabels: true,
  w: 600,
  h: 600
};

var mapInstance = new FuseMap(configMap);
</script>
	
```

5) As on optimization you are encouraged to concatenate and minify your script files using something like uglify. 

## API

###Initialise

`var fuseMapInstance = new FuseMap(config);`
 
 
Configuration

```
config = {
  selector: 'map',            // (string) id of the DOM Element where to draw the map,
  fuseData: data,             // (obj || string) data object from fuse or url to query fuse,
  mapData: dataObj,           // (obj || string) topojson map data object or url to fetch such data
  mapPropertyId: 'id'         // (string) The main property that identifies the map data, ex. 'COUNTYFP' for counties
  colorBrewerScheme: array    // Array of rgb colours, check colorbrewer for nice schemes
  showTooltip: true,          // (bool) Enable tooltips on hover,
  showLimitLines: true,       // (bool) Enable geographic limits (like county or state lines),
  showLabels: true,           // (bool) Enable label names (states, counties),
  w: 600,                     // (number) width of the map in pixels,
  h: 600                      // (number) height of the map in pixels
};
```
 
### FuseMap data

FuseMap requires two sets of data, the data coming from Fuse and the geographic data processed with topojson.

In both cases it is possible to supply the data (in case is already present in the app) or to provide an URL to where to get the data, both options need to provide a JSON response. FuseMap will detect each case and act accordingly.

### Color schemes

For this option you need to provide an array of rgb colours, the best option is to go to http://colorbrewer2.org/ and get them from there.

### Tooltip

The tooltips are easily styled, use the class: 'tooltip'.
 
####Tooltip markup

```
<div class="tooltip">
  <header><%= name %></header>
  <section>
    Frequency: <strong><%= freq %></strong>
  </section>
</div>
```
---

## Charts

To create charts (line, bar, pie, etc) use [FuseViz](https://bitbucket.org/jpablolomeli/fuseviz)

---

## Changelog

#### 18 Aug 2014
**v0.1.0**
- Initial version made available!