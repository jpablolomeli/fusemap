(function() {
  'use strict';

  var configMap = {
    selector: 'maps',
    fuseData: 'data/mapData.json',
    mapData: 'maps/cb_2013_us_county_500k.topo.json',
    mapPropertyId: 'COUNTYFP',
    colorBrewerScheme: [
      'rgb(240,249,232)',
      'rgb(123,204,196)',
      'rgb(8,104,172)'
    ],
    showTooltip: true,
    showLimitLines: true,
    showLabels: true,
    w: 600,
    h: 600
  };

  var mapInstance = new FuseMap(configMap);

})();
