'use strict';

var FuseMap = function (config) {

  // a) Validate properties
  // ---------------------------------------------------
  this.validateConfig(config);

  // b) Configure
  // ---------------------------------------------------
  // 1. User configuration
  this.selector = config.selector;
  this.colorScheme = config.colorBrewerScheme;
  this.showTooltip = config.showTooltip;
  this.showLimitLines = config.showLimitLines;
  this.showLabels = config.showLabels;
  this.mapPropertyId = config.mapPropertyId;
  this.w = config.w;
  this.h = config.h;

  if (_.isString(config.fuseData)) {
    this.fuseDataUrl = config.fuseData;
  }
  else {
    this.fuseData = config.fuseData;
  }
  if (_.isString(config.mapData)) {
    this.mapDataUrl = config.mapData;
  }
  else {
    this.mapData = config.mapData;
  }
  // c) Initialise
  // ---------------------------------------------------

  this.configureMap();
  this.drawMap();
};

FuseMap.prototype.validateConfig = function(config) {
  var Exception = function(value, message) {
    this.value = value;
    this.message = message;
    this.toString = function() {
      return this.value + ': ' + this.message;
    };
  };

  if (_.isUndefined(window.topojson)) {
    throw new Exception(window.topojson, 'topojson is a dependency and it has not been included');
  }

  if (_.isUndefined(config.selector)) {
    throw new Exception(config.selector, 'A valid element ID has not been provided');
  }

  if (_.isUndefined(config.fuseData)) {
    throw new Exception(config.fuseData, 'You have to provide fuseData or the url to query fuse');
  }

  if (_.isUndefined(config.mapData)) {
    throw new Exception(config.mapData, 'You have to provide valid topojson mapData or the url where it resides');
  }
};

FuseMap.prototype.configureMap = function () {
  this.projection = d3.geo.mercator()
    .center([-72, 43])
    .rotate([4.4, 0])
    .scale(4500)
    .translate([this.w / 2, this.h / 2]);

  this.path = d3.geo.path().projection(this.projection);

  this.svg = d3.select('#' + this.selector).append('svg').attr({width: this.w, height: this.h});

  this.color = d3.scale.linear()
    .range(this.colorScheme);

  if (this.showTooltip) {
    this.tooltip = d3.select('body').append('div')
      .attr('class', 'tooltip')
      .style('opacity', 0);
    this.tooltipHtml = _.template('<div class="tooltip"><header><%= name %></header><section>Frequency: <strong><%= freq %></strong></section></div>');
  }

};

FuseMap.prototype.setColorDomain = function (fuseItems) {
  this.color.domain([
    0, d3.max(fuseItems.items, function (d) {
      return d.freq;
    })
  ]);
};

FuseMap.prototype.getTopojsonFeatures = function (mapJson) {
  for(var k in mapJson.objects) {
    this.key = k;
  }
  return topojson.feature(mapJson, mapJson.objects[this.key]);
};

FuseMap.prototype.drawMapLines = function (mapJson) {
  this.svg.append('path')
    .datum(topojson.mesh(mapJson, mapJson.objects[this.key], function (a, b) {
      return a !== b;
    }))
    .attr('d', this.path)
    .attr({
      fill: 'none',
      stroke: '#777',
      'stroke-dasharray': 2.2,
      'stroke-linejoin': 'round'
    });
};

FuseMap.prototype.insertLabels = function (mapJson) {
  var that = this;

  this.svg.selectAll('.place-label')
    .data(topojson.feature(mapJson, mapJson.objects[this.key]).features)
    .enter()
    .append('text')
    .text(function (d) {
      return d.properties.NAME;
    })
    .attr({
      x: function (d) {
        return that.path.centroid(d)[0];
      },
      y: function (d) {
        return  that.path.centroid(d)[1];
      },
      'text-anchor': 'middle',
      'class': 'place-label',
      'font-size': '9px',
      'font-family': 'sans-serif'
    });
};

FuseMap.prototype.mergeData = function (fuseItems, topojsonFeatures) {

  var that = this;

  _.forEach(fuseItems.items, function (item) {

    var geoName = item.value.split(',')[1];

    _.forEach(topojsonFeatures, function (feat) {

      if (geoName === feat.properties[that.mapPropertyId]) {
        feat.properties.freq = parseInt(item.freq);
      }

    });

  });
};

FuseMap.prototype.processD3Map = function (topojsonFeatures, mapJson) {
  var that = this;

  that.svg.selectAll('path')
    .data(topojsonFeatures)
    .enter()
    .append('path')
    .attr({
      'd': that.path,
      'class': 'unit-name'
    })
    .style('fill', function (d) {

      var freq = d.properties.freq;

      if (freq) {
        return(that.color(freq));
      }
      else {
        return '#fff';
      }
    })
    .on('mouseover', function (d) {
      if (that.showTooltip) {
        that.tooltip.transition()
          .duration(400)
          .style('opacity', 0.9);

        that.tooltip.html(that.tooltipHtml({
          name: d.properties.NAME,
          freq: d.properties.freq
        }))
          .style('left', (d3.event.pageX) + 'px')
          .style('top', (d3.event.pageY - 30) + 'px');
      }
    })
    .on('mouseout', function () {
      if (that.showTooltip) {
        that.tooltip.transition()
          .duration(400)
          .style('opacity', 0);
      }
    });
  if (that.showLimitLines) {
    that.drawMapLines(mapJson);
  }
  if (that.showLabels) {
    that.insertLabels(mapJson);
  }
};

FuseMap.prototype.drawMap = function () {
  //  Cache this
  var that = this;

  //  Check if we have a url for the FuseData, if we do, make a d3 json call
  if (!_.isUndefined(that.fuseDataUrl)) {

    d3.json(that.fuseDataUrl, function (fuseItems) {

      that.setColorDomain(fuseItems);

      //  Check if we have a url for the mapData, if we do, make a d3 json call
      if (!_.isUndefined(that.mapDataUrl)) {

        d3.json(that.mapDataUrl, function (mapJson) {

          var topojsonFeatures = that.getTopojsonFeatures(mapJson).features;
          that.mergeData(fuseItems, topojsonFeatures);
          that.processD3Map(topojsonFeatures, mapJson);

        });
      }
      else {
        var topojsonFeatures = that.getTopojsonFeatures(that.mapData).features;
        that.mergeData(fuseItems, topojsonFeatures);
        that.processD3Map(topojsonFeatures, that.mapData);
      }
    });

  }
  else if(_.isObject(that.fuseData)) {
    that.setColorDomain(that.fuseData);

    //  Check if we have a url for the mapData, if we do, make a d3 json call
    if (!_.isUndefined(that.mapDataUrl)) {

      d3.json(that.mapDataUrl, function (mapJson) {

        var topojsonFeatures = that.getTopojsonFeatures(mapJson).features;
        that.mergeData(that.fuseData, topojsonFeatures);
        that.processD3Map(topojsonFeatures, mapJson);

      });
    }
    else {
      var topojsonFeatures = that.getTopojsonFeatures(that.mapData).features;
      that.mergeData(that.fuseData, topojsonFeatures);
      that.processD3Map(topojsonFeatures, that.mapData);
    }
  }
};
